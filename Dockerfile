FROM cloudron/base:3.0.0@sha256:455c70428723e3a823198c57472785437eb6eab082e79b3ff04ea584faf46e92

WORKDIR /app/code
ARG STATPING_VERSION=0.90.74
RUN curl -L https://github.com/statping/statping/releases/download/v$STATPING_VERSION/statping-linux-amd64.tar.gz | tar -xz -f -

#RUN mkdir -p /app/pkg/ /app/data/ && ln -sfn /app/data/assets /app/pkg/assets

COPY config.yml.template start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
