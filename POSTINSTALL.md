This app is pre-setup with an admin account. The initial credentials are:

**Username**: admin<br/>
**Password**: admin<br/>

Please change the admin password immediately.

When making config or setting changes, go to admin settings and clear the
cache for the graphs to appear.
