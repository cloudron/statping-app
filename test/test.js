#!/usr/bin/env node

/* global describe */
/* global before */
/* global after */
/* global it */

'use strict';

require('chromedriver');

var execSync = require('child_process').execSync,
    expect = require('expect.js'),
    path = require('path'),
    { Builder, By, Key, until } = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Application life cycle test', function () {
    this.timeout(0);

    var app;
    var server, browser = new Builder().forBrowser('chrome').build();

    const USERNAME = 'admin';
    const PASSWORD = 'admin';
    const LOCATION = 'test';
    const TEST_TIMEOUT = 100000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const MONITOR_NAME = 'cloudron';
    const MONITOR_URL = 'https://cloudron.io';

    before(function () {
        var seleniumJar= require('selenium-server-standalone-jar');
        var SeleniumServer = require('selenium-webdriver/remote').SeleniumServer;
        server = new SeleniumServer(seleniumJar.path, { port: 4444 });
        server.start();
    });

    after(function () {
        browser.quit();
    });

    function waitForElement(elem) {
        return browser.wait(until.elementLocated(elem), TEST_TIMEOUT).then(function () {
            return browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
        });
    }

    function login(callback) {
        browser.manage().deleteAllCookies();
        browser.get(`https://${app.fqdn}/dashboard`).then(function () {
            return waitForElement(By.id('username'));
        }).then(function () {
            return browser.findElement(By.id('username')).sendKeys(USERNAME);
        }).then(function () {
            return browser.findElement(By.id('password')).sendKeys(PASSWORD);
        }).then(function () {
            return browser.findElement(By.xpath('//button[@type="submit"]')).submit();
        }).then(function () {
            return waitForElement(By.xpath('//span[text()="Total Services"]'));
        }).then(function () {
            callback();
        });
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION || a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    }

    function createMonitor(done) {
        browser.get(`https://${app.fqdn}/dashboard/create_service`).then(function () {
            return waitForElement(By.id('name'));
        }).then(function () {
            return browser.findElement(By.id('name')).sendKeys(MONITOR_NAME);
        }).then(function () {
            return browser.findElement(By.id('service_url')).sendKeys(MONITOR_URL);
        }).then(function () {
            return browser.findElement(By.xpath('//button[@type="submit"]')).click();
        }).then(function () {
            return waitForElement(By.xpath('//a[@href="/dashboard/create_service"]'));
        }).then(function () {
            done();
        });
    }

    function checkDomainConfiguration(done) {
        browser.get(`https://${app.fqdn}/dashboard/settings`).then(function () {
            return waitForElement(By.id('domain'));
        }).then(function () {
            return browser.findElement(By.id('domain')).getAttribute('value');
        }).then(function (text) {
            if (text !== `https://${app.fqdn}`) return done(new Error('domain is not properly updated: ' + text));
            done();
        });
    }

    function monitorExists(done) {
        browser.get(`https://${app.fqdn}`).then(function () {
            return waitForElement(By.xpath(`//a[text()="${MONITOR_NAME}"]`));
        }).then(function () {
            done();
        });
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync('cloudron install --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login', login);
    it('check domain configuration', checkDomainConfiguration);
    it('can create monitor', createMonitor);
    it('monitor exists', monitorExists);

    it('can restart app', function () { execSync(`cloudron restart --app ${app.id}`, EXEC_ARGS); });
    it('monitor exists', monitorExists);

    it('backup app', function () { execSync(`cloudron backup create --app ${app.id}`, EXEC_ARGS); });
    it('restore app', function () {
        const backups = JSON.parse(execSync('cloudron backup list --raw'));
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        execSync('cloudron install --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });
    it('monitor exists', monitorExists);
    it('check domain configuration', checkDomainConfiguration);

    it('move to different location', function (done) {
        // ensure we don't hit NXDOMAIN in the mean time
        browser.get('about:blank').then(function () {
            execSync(`cloudron configure --location ${LOCATION}2 --app ${app.id}`, EXEC_ARGS);
            done();
        });
    });

    it('can get app information', getAppInfo);
    it('can login', login);
    it('monitor exists', monitorExists);
    it('check domain configuration', checkDomainConfiguration);

    it('uninstall app', function (done) {
        // ensure we don't hit NXDOMAIN in the mean time
        browser.get('about:blank').then(function () {
            execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
            done();
        });
    });

    // test update
    it('can install app', function () {
        execSync('cloudron install --appstore-id ' + app.manifest.id + ' --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can get app information', getAppInfo);
    it('can login', login);
    it('can create monitor', createMonitor);
    it('monitor exists', monitorExists);
    it('can update', function () { execSync(`cloudron update --app ${LOCATION}`, EXEC_ARGS); });
    it('can login', login);
    it('monitor exists', monitorExists);
    it('check domain configuration', checkDomainConfiguration);
    it('uninstall app', function (done) {
        // ensure we don't hit NXDOMAIN in the mean time
        browser.get('about:blank').then(function () {
            execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
            done();
        });
    });
});
