#!/bin/bash

set -eu

# env vars are supposed to take precedence over .env and config.yml
# however, if use env var then get written into config.yml and in future runs they are ignored
# values set in ui are also stored in the database in the core table as a single row
# the docs seem to be written for docker based setup where config.yml is not persistent probably
# https://github.com/statping/statping/wiki/Environment-Variables

# migrate secret from any old .env file
if [[ -f /app/data/.env ]]; then
    api_secret=$(sed -n 's/API_SECRET=\(.*\)/\1/p' /app/data/.env)
    rm -rf /app/data/.env
else
    api_secret=$(pwgen -1s 32)
fi

if [[ ! -f /app/data/config.yml ]]; then
    sed -e "s/##API_SECRET/${api_secret}/g" /app/pkg/config.yml.template > /app/data/config.yml

    # these values are only useful for first run it seems
    export DOMAIN=$CLOUDRON_APP_ORIGIN
    export SAMPLE_DATA=true
else
    # settings are stored in a single row!
    PGPASSWORD=${CLOUDRON_POSTGRESQL_PASSWORD} psql -h ${CLOUDRON_POSTGRESQL_HOST} -p ${CLOUDRON_POSTGRESQL_PORT} -U ${CLOUDRON_POSTGRESQL_USERNAME} -d ${CLOUDRON_POSTGRESQL_DATABASE} -t -c "UPDATE core SET domain='${CLOUDRON_APP_ORIGIN}';"
fi

yq eval ".connection=\"postgres\"" -i /app/data/config.yml
yq eval ".host=\"$CLOUDRON_POSTGRESQL_HOST\"" -i /app/data/config.yml
yq eval ".user=\"$CLOUDRON_POSTGRESQL_USERNAME\"" -i /app/data/config.yml
yq eval ".password=\"$CLOUDRON_POSTGRESQL_PASSWORD\"" -i /app/data/config.yml
yq eval ".database=\"$CLOUDRON_POSTGRESQL_DATABASE\"" -i /app/data/config.yml
yq eval ".port=$CLOUDRON_POSTGRESQL_PORT" -i /app/data/config.yml

echo "=> Ensure permissions"
chown -R cloudron:cloudron /app/data

echo "=> Start the statping"
exec /usr/local/bin/gosu cloudron:cloudron /app/code/statping --config /app/data/config.yml
