# Statping app for Cloudron

Since the Statping does not integrate with the user management of Cloudron and its default password is simply "admin" the app generates a random password on first start.

Run the following command on your local system after installing the app to print out the password:

```
cloudron exec grep ADMIN_PASSWORD= .env
```

TODO

- [x] add sass for style changes (the current binary does not work)


https://pastebin.com/69pN5Emx
https://pastebin.com/raw/bdwjz9E5
https://pastebin.com/raw/Lr6KDX0W
